# Inventory Calendar for Hotel

This is a Single Page Application for managing inventory of hotel rooms to set their capacity and price on daily basis for each room types (single/double). For simplicity we have a default hotel with IDR as its currency already seeded to database so all the inventory items are attributed to this hotel.

The technologies used are PHP7, Lumen, Blade template, Sqlite, Vuejs, Fetch, HTML and CSS.

Usage is pretty simple, the home page loads the calendar interface where the data can be entered/edited on per day basis or in bulk.

# Installation

- Git clone this repo `git clone git@gitlab.com:adh0core/hotel-inventory.git` (or `git clone https://gitlab.com/adh0core/hotel-inventory.git` if you are not logged in to gitlab)
- `cd hotel-inventory` and run `composer install`
- Server requirement PHP7+ with pdo-sqlite and mbstring extensions.
- If you want to try development preview with PHP built in server then run `php -S localhost:8080 -t public`

# Configuration

The application will clone `.env.example` to `.env`. You can change the parameters in `.env` to suit your needs and environment.

# Endpoints
The app for now only has three API endpoints (besides the obvious landing page) that give JSON response:

- `GET /inventory/monthly?date=<YYYY-MM>` - To fetch inventories of a month grouped by each day and room type.
- `POST /inventory/bulk` - To update the price and availability for a wide range of dates at once.
- `PUT /inventory` - To update the price or availability for a day.

Both the update endpoints accept JSON encoded request payload.
