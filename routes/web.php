<?php

/*
 * Application Routes
*/

$router->get('/', [
    'as'   => 'home',
    'uses' => 'InventoryController@showHome',
]);

$router->group(['prefix' => '/inventory'], function ($router) {
    $router->get('/{hotelId}/monthly', [
        'as'   => 'monthly',
        'uses' => 'InventoryController@listMonthly',
    ]);

    $router->post('/{hotelId}/bulk', [
        'as'   => 'bulk.update',
        'uses' => 'InventoryController@updateBulk',
    ]);

    $router->put('/{hotelId}', [
        'as'   => 'update',
        'uses' => 'InventoryController@update',
    ]);
});
