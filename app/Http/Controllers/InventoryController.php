<?php

namespace App\Http\Controllers;

use App\Repo\Inventory;

use Laravel\Lumen\Routing\Controller as BaseController;

class InventoryController extends BaseController
{
    public function showHome()
    {
        return view('home', [
            'title'   => 'Hotel Room Inventory Calendar',
            'hotelId' => app('db')->table('hotel')->first(['id'])->id,
        ]);
    }

    public function listMonthly(int $hotelId)
    {
        $date = app('request')->get('date') ?: date('Y-m');

        return response()->json(
            app(Inventory::class)->getMonthlyData($date, $hotelId)
        );
    }

    public function updateBulk(int $hotelId)
    {
        $data = app('request')->json()->all() ?: [];

        return response()->json(
            app(Inventory::class)->updateMultipleRows($data, $hotelId)
        );
    }

    public function update(int $hotelId)
    {
        $data = app('request')->json()->all() ?: [];

        return response()->json(
            app(Inventory::class)->updateSingleRow($data, $hotelId)
        );
    }
}
