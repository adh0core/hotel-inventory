<?php

namespace App\Repo;

/**
 * Inventory repository that includes db queries and minor business logics.
 *
 * @author Jitendra Adhikari <jiten.adhikary@gmail.com>
 */
class Inventory
{
    /**
     * Get monthly inventory data grouped by each of the days in the month.
     *
     * @param  string      $date    Date in the format 'Y-m'
     * @param  int|integer $hotelId The hotel to which data is attributed to.
     *
     * @return array Eg: ['YYYY-MM-DD' => ['single' => ['currency' => 'IDR', 'price' => '1000', 'availability' => 1]]]
     *
     * @throws \InvalidArgumentException When date is invalid (valid range 2000-01 to 2099-12).
     */
    public function getMonthlyData(string $date, int $hotelId): array
    {
        if (!preg_match('/^20\d{2}\-(0?[1-9]|1[012])$/', $date)) {
            throw new \InvalidArgumentException('The given date is not valid.');
        }

        $first = $date . '-01';
        $days  = date('t', strtotime($date . '-01 00:00:00'));
        $last  = $date . '-' . $days;

        $result = [];
        $sample = [
            'price'        => 0,
            'availability' => 0,
            'currency'     => 'N/A',
        ];

        // Add default for each day!
        for ($i = 1; $i <= $days; $i++) {
            $day = $date . '-' . ($i < 10 ? '0' . $i : $i);
            $result[$day] = ['single' => $sample, 'double' => $sample];
        }

        // Insert actual value if available!
        $data = app('db')->table('inventory as i')
            ->select('i.availability', 'i.price', 'i.room_type', 'i.effective_date', 'c.iso_3 as currency')
            ->leftJoin('hotel as h', 'h.id', '=', 'i.hotel_id')
            ->leftJoin('currency as c', 'c.id', '=', 'h.currency_id')
            ->whereBetween('i.effective_date', [$first, $last])
            ->where('h.id', '=', $hotelId)
            ->get();

        foreach ($data as $row) {
            $row = (array) $row;
            $result[$row['effective_date']][$row['room_type']] = [
                'price'        => $row['price'] ?? 0,
                'availability' => $row['availability'] ?? 0,
                'currency'     => $row['currency'] ?? 'N/A',
            ];
        }

        return $result;
    }

    /**
     * Bulk insert/update data for a given date range and hotel.
     *
     * @param  array       $data
     * @param  int|integer $hotelId
     *
     * @return bool
     *
     * @throws \InvalidArgumentException When the data is incomplete.
     */
    public function updateMultipleRows(array $data, int $hotelId): bool
    {
        // Validate
        if (!isset(
            $data['effective_date_from'],
            $data['effective_date_to'],
            $data['room_type'],
            $data['availability'],
            $data['price']
        )) {
            throw new \InvalidArgumentException(
                'All of the following fields are required: effective_date, room_type, availability, price'
            );
        }

        $from = strtotime($data['effective_date_from'] . ' 00:00:00');
        $to   = strtotime($data['effective_date_to']);

        if ($from > $to) {
            list($from, $to) = [$to, $from];
        }

        if (($to - $from) / 86400 > 60) {
            throw new \InvalidArgumentException(
                'The date range is too long, please limit within 60 days'
            );
        }

        $filter = !empty($data['filter_days'])
            ? explode(',', $data['filter_days'])
            : ['*'];

        unset($data['effective_date_from'], $data['effective_date_to'], $data['filter_days']);

        while ($from <= $to) {
            if (in_array('*', $filter) || in_array(date('w', $from), $filter)) {
                $data['effective_date'] = date('Y-m-d', $from);

                $this->updateSingleRow($data, $hotelId);
            }

            // Add a day (86400 seconds)!
            $from += 86400;
        }

        return true;
    }

    /**
     * Insert/update a single row for given hotel.
     *
     * @param  array  $data
     * @param  int    $hotelId
     *
     * @return bool
     *
     * @throws \InvalidArgumentException When the data is incomplete.
     */
    public function updateSingleRow(array $data, int $hotelId): bool
    {
        // Validate
        if (!isset($data['effective_date'], $data['room_type']) || (
            !isset($data['price']) && !isset($data['availability'])
        )) {
            throw new \InvalidArgumentException(
                'effective_date and/or one of price and availability missing'
            );
        }

        $data['hotel_id'] = $hotelId;

        $updates = array_intersect_key($data, ['price' => 1, 'availability' => 1]);
        unset($data['price'], $data['availability']);

        return app('db')->table('inventory')->updateOrInsert($data, $updates);
    }
}
