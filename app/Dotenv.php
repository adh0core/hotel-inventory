<?php

namespace App;

/**
 * Light fast Dotenv parser.
 *
 * @author Jitendra Adhikari <jiten.adhikary@gmail.com>
 */
class Dotenv
{
    /**
     * Loads .env file as and puts the key value pair in ENV. Use `getenv('THE_KEY')` to retrieve.
     *
     * @param string $file     The full path of .env file.
     * @param bool   $override Whether to override already available env key.
     *
     * @throws \ErrorException   If the file does not exist or cant be read.
     * @throws \RuntimeException If the file content cant be parsed.
     */
    public static function load(string $file, bool $override = false)
    {
        if (!is_file($file)) {
            throw new \ErrorException('The .env file doesnot exist or is not readable.');
        }

        // Get file contents, fix the comments and parse as ini.
        $content = preg_replace('/^\s*#/m', ';', file_get_contents($file));
        $parsed  = parse_ini_string($content, false, \INI_SCANNER_TYPED);

        if ($parsed === false) {
            throw new \RuntimeException('The .env file cannot be parsed due to malformed values.');
        }

        foreach ($parsed as $key => $value) {
            if ($override || getenv($key) === false) {
                putenv("$key=$value");
            }
        }
    }
}
