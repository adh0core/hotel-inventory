@extends('layout')

@section('content')
<div id="app" class="container app">
<div class="bulk-operation">
  <form class="form-horizontal" method="POST" v-on:submit.prevent="bulkUpdate">
    <div class="row bg-dark-gray">
      <div class="col-md-12">
        <div class="dotted-bottom">Bulk Operations</div>
      </div>
    </div>
    <div class="row bg-light-gray">
      <div class="form-group col-md-4">
        <div class="col-md-4">
          <label for="room-type">Select Room:</label>
        </div>
        <div class="col-md-6">
          <select v-model="roomType" class="form-control input-sm" id="room-type">
            <option v-for="(typeDetail, roomType) in roomTypes" :value="roomType" :inventory="typeDetail.inventory">
              [[ typeDetail.label ]]
            </option>
          </select>
        </div>
      </div>
    </div>
    <div class="row bg-dark-gray">
      <div class="form-group">
        <div class="col-md-4">
          <div class="col-md-4">
            <label>Select Days:</label>
          </div>
          <div class="col-md-8">
            <div class="row">
              <div class="col-md-2">
                <label class="no-bold" for="from-date">From:</label>
              </div>
              <div class="col-md-10">
                <input class="form-control input-sm" v-model="fromDate" type="date" name="from-date" id="from-date" required placeholder="YYYY-MM-DD" />
              </div>
            </div>
            <div class="row">
              <div class="col-md-2">
                <label class="no-bold" for="to-date">To:</label>
              </div>
              <div class="col-md-10">
                <input class="form-control input-sm" v-model="toDate" type="date" name="to-date" id="to-date" required placeholder="YYYY-MM-DD" />
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-7">
          <div class="row">
            <div class="col-md-2">
              <label class="no-bold">Refine Days:</label>
            </div>
            <div class="col-md-10">
            <template v-for="(day, pos) in refineDays">
              <div :class="'col-md-3 ' + (pos == 7 ? 'clear' : '')">
                <input type="checkbox" v-model="filterDays" :value="day.value" :id="'refine-day-' + pos" />
                <label class="no-bold" :for="'refine-day-' + pos">[[ day.label ]]</label>
              </div>
            </template>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row bg-light-gray">
      <div class="form-group col-md-5">
        <div class="col-md-5">
          <label for="bulk-price">Change Price To:</label>
        </div>
        <div class="col-md-7">
          <input class="form-control input-sm" v-model="bulkPrice" type="number" min="0" id="bulk-price" required />
        </div>
      </div>
    </div>
    <div class="row bg-light-gray top-less">
      <div class="form-group col-md-5">
        <div class="col-md-5">
          <label for="bulk-availability">Change Availability To:</label>
        </div>
        <div class="col-md-7">
          <input class="form-control input-sm" v-model="bulkAvailability" type="number" id="bulk-availability" min="0" :max="roomTypes[roomType].inventory" required />
        </div>
      </div>
    </div>
    <div class="row bg-dark-gray with-bottom">
      <div class="col-md-2">
        <button type="button" class="btn btn-sm btn-default" v-on:click="cancel">Cancel</button>
        <button type="submit" class="btn btn-sm btn-success">Update</button>
      </div>
      <div class="col-md-10 feedback">
        <span v-show="errors" class="text-danger">[[ errors ]]</span>
        <span v-show="saved" class="text-success">Saved</span>
      </div>
    </div>
    <div class="spacer-10">&nbsp;</div>
  </form>
</div>
<div class="spacer-10">&nbsp;</div>
<calendar :room-types="roomTypes" :storage="storage" ref="calendar"></calendar>
</div>
@include('partial/calendar')
@include('partial/prompt')
@endsection
