<!DOCTYPE html>
<html lang="en">
<head>
  <title>{{ $title }}</title>
  <meta charset="utf-8">
  <meta name="author" content="Jitendra Adhikari">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
@if (app()->environment('production'))
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
@else
  <link rel="stylesheet" href="css/vendor/bootstrap.min.css"/>
@endif
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
@yield('content')
@include('partial/script')
</body>
</html>
