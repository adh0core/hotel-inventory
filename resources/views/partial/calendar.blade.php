<script type="text/x-template" id="calendar-grid">
<div class="row calendar">
  <div class="calendar-head">
    <table class="full">
      <thead>
        <tr class="title-main">
          <th class="bottom-less bg-dark-gray cozy">
            <div class="title">Price and Availability</div>
          </th>
          <th class="bg-gray border-left cozy">
            <div class="title-month">
              <span class="caret pointer down" v-on:click="subMonth"></span>
              <span class="dropup"><span class="caret pointer up" v-on:click="addMonth"></span></span>
              <span class="month">[[ month ]] [[ year ]]</span>
              <span class="caret pointer left" v-on:click="subYear"></span>
              <span class="caret pointer right" v-on:click="addYear"></span>
            </div>
          </th>
        </tr>
        <tr>
          <td>
            <span class="caret pointer left pull-right scroller" v-on:click="scrollLeft"></span>
            <span class="caret pointer right pull-right scroller" v-on:click="scrollRight"></span>
          </td>
        </tr>
      </thead>
    </table>
  </div>
  <div class="calendar-data scrollable" id="calendar-data">
    <table>
      <thead>
        <tr>
          <th class="bg-dark-gray"><p class="title">&nbsp;</p></th>
          <th v-for="day in daysOfWeek" :class="'compact border-left ' + day.type">[[ day.name ]]</th>
        </tr>
        <tr>
          <th class="bg-dark-gray"><p class="title">&nbsp;</p></th>
          <td v-for="day in daysOfMonth" class="bg-gray border-left">[[ day.monthDay ]]</td>
        </tr>
      </thead>
      <tbody>
        <tr><td><prompt ref="prompt"></prompt></td></tr>
        <template v-for="(typeDetail, roomType) in roomTypes">
          <tr :class="roomType">
            <th :colspan="totalDays + 1" class="text-left cozy">[[ typeDetail.label ]]</th>
          </tr>
          <tr>
            <td class="bottom-less bg-dark-gray">
              <p class="title">Rooms available</p>
            </td>
            <td v-for="(data, fullDate) in monthlyData" class="bg-dark-gray border-all">
              <div class="value">
                <span class="dotted-bottom" v-on:click="showPrompt" :data-value="data[roomType].availability"
                  data-prop="availability" :data-room-type="roomType" :data-full-date="fullDate"
                  :data-max="roomTypes[roomType].inventory">
                  [[ data[roomType].availability ]]
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td class="top-less bg-dark-gray">
              <p class="title">Price</p>
            </td>
            <td v-for="(data, fullDate) in monthlyData" class="border-all">
              <div class="value">
                <span class="dotted-bottom" v-on:click="showPrompt" :data-value="data[roomType].price"
                  data-prop="price" :data-room-type="roomType" :data-full-date="fullDate">
                  [[ data[roomType].price ]]
                </span>
                <span>[[ data[roomType].currency ]]</span>
              </div>
            </td>
          </tr>
        </template>
      </tbody>
    </table>
  </div>
</div>
</script>
