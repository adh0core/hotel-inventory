<script type="text/x-template" id="prompt">
  <div class="prompt clear" v-show="opened">
   <input :class="'prompt-input ' + (error ? 'error' : '')" min="0" :max="data.max" type="number" :value="newValue" v-model="newValue" />
    <button type="submit" class="btn btn-sm btn-info" v-on:click="update">&#10004;</button>
    <button type="button" class="btn btn-sm btn-default" v-on:click="close">&times;</button>
    <span class="caret"></span>
  </div>
</script>
