<script type="text/javascript">
  const MONTHLY_DATA_URL  = '{{ route('monthly', compact('hotelId')) }}';
  const SINGLE_UPDATE_URL = '{{ route('update', compact('hotelId')) }}';
  const BULK_UPDATE_URL   = '{{ route('bulk.update', compact('hotelId')) }}';
</script>
@if (app()->environment('production'))
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.2/vue.min.js"></script>
@else
<script src="js/vendor/moment.min.js"></script>
<script src="js/vendor/vue.js"></script>
@endif
<script src="js/vendor/fetch.js"></script>
<script src="js/app.js"></script>
