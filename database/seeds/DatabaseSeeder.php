<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Currency
        app('db')->table('currency')->insert([
            'iso_3' => 'IDR',
            'name' => 'Indonesian Rupiah',
        ]);

        // Hotel
        app('db')->table('hotel')->insert([
            'currency_id' => 1,
            'name' => 'Grand Budapest Hotel',
        ]);
    }
}
