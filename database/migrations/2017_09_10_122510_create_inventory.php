<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hotel_id');
            $table->string('room_type', 25);
            $table->unsignedTinyInteger('availability')->nullable();
            $table->unsignedDecimal('price')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('effective_date');
            $table->foreign('hotel_id')->references('id')->on('hotel');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
