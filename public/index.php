<?php

/*
 * Create and Run the Application
*/

chdir(__DIR__ . '/../');

$app = require __DIR__ . '/../bootstrap/app.php';

// Sqlite REQUIRES full db path but we have a relative one!
if (getenv('DB_CONNECTION') === 'sqlite' && !is_file(getenv('DB_DATABASE'))) {
    putenv('DB_DATABASE=' . __DIR__ . '/../' . getenv('DB_DATABASE'));
}

$app->run();
