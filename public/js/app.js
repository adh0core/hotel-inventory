/**
 * @author Jitendra Adhikari <jiten.adhikary at gmail dot com>
 */
const Store = function (prefix) {
  return {
    getCurrentDate: function () {
      return localStorage.getItem(prefix + 'current-date');
    },

    setCurrentDate: function (value) {
      localStorage.setItem(prefix + 'current-date', value);
    },
  }
};

const Http = {
  call: function (fetchArgs, success, failure) {
    if (typeof failure !== 'function') {
      failure = function (msg) { alert(msg); };
    }

    const onFailure = function (response) {
      if (response instanceof Response) {
        response.json().then(function (json) {
          failure(json.error || 'Something went wrong!');
        });
      } else {
        failure(response instanceof Error ? response.message : response);
      }
    };

    if (!fetchArgs) fetchArgs = {};

    let headers;
    if (headers = fetchArgs.headers) {
      if (!headers instanceof Headers) headers = new Headers(headers);
    } else {
      headers = new Headers;
    }

    headers.append('X-Requested-With', 'XMLHttpRequest');
    fetchArgs.headers = headers;

    fetch(fetchArgs.url, fetchArgs).then(function (response) {
      if (response.status < 200 || response.status > 399) {
        onFailure(response);
      } else if (typeof success === 'function') {
        response.json().then(success);
      }
    }).catch(onFailure);
  }
}

const Prompt = Vue.component('prompt', {
  template: '#prompt',

  delimiters: ['[[', ']]'],

  data: function () {
    return { opened: false, newValue: null, data: {}, max: null, error: false };
  },

  methods: {
    open: function (data, pos) {
      this.data = data;
      this.newValue = data.value;

      this.$el.style.left = (pos.left - 118) + 'px';
      this.$el.style.top  = (pos.top - 58) + 'px';
      this.opened = true;
    },

    close: function () {
      this.error  = false;
      this.opened = false;
    },

    update: function (event) {
      const elem = (event.currentTarget || event.target).parentNode;

      if (+this.newValue < 1 || isNaN(+this.newValue) || (this.data.max && this.newValue > this.data.max)) {
        return this.error = true;
      }

      this.error = false;

      // No change!
      if (this.data.value === this.newValue) {
        return this.close();
      }

      const self = this;
      const data = {
        effective_date: this.data.fullDate,
        room_type: this.data.roomType,
      };

      data[this.data.prop] = this.newValue;

      Http.call({
        url: SINGLE_UPDATE_URL, method: 'put', body: JSON.stringify(data)
      }, function () {
        self.$parent.$options.methods.updated.call(self.$parent, self.newValue)
        self.close(event, true);
      });
    }
  }

});

const Calendar = Vue.component('calendar', {
  template: '#calendar-grid',

  delimiters: ['[[', ']]'],

  props: [
    'storage',
    'roomTypes',
  ],

  data: function () {
    const date = this.storage.getCurrentDate() || moment().format('YYYY-MM');

    return {
      date: date,
      monthlyData: {},
      moment: moment(date, 'YYYY-MM'),
      currentElem: null,
    };
  },

  computed: {
    month: function () {
      return this.moment.format('MMMM');
    },

    year: function () {
      return this.moment.format('YYYY');
    },

    totalDays: function () {
      return this.moment.daysInMonth();
    },

    daysOfWeek: function () {
      const weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
      const weekEnds = { 0: 'weekend', 6: 'weekend' };
      const total = this.moment.daysInMonth();

      let days = [];
      let startIdx = weekDays.indexOf(this.moment.format('dddd'));

      for (let day = 1; day <= total; day++) {
        days.push({
          name: weekDays[startIdx % 7],
          type: weekEnds[startIdx % 7] || 'weekday'
        });
        startIdx++;
      }

      return days;
    },

    daysOfMonth: function () {
      const total = this.moment.daysInMonth();
      let days = [];

      for (let day = 1; day <= total; day++) {
        days.push({ monthDay: day });
      }

      return days;
    },
  },

  created: function () {
    this.fetchData();
  },

  watch: {
    date: function () {
      this.moment = moment(this.date, 'YYYY-MM');

      this.storage.setCurrentDate(this.date);

      this.fetchData.call(this);
    }
  },

  methods: {
    fetchData: function () {
      const self = this;
      const url  = MONTHLY_DATA_URL + '?date=' + self.date;

      Http.call({ url: url }, function (json) {
          self.monthlyData = json;
      });
    },

    fetchIfOverlap: function (from, to) {
      const thisFrom = this.moment.format('x');
      const thisTo = moment(this.date + '-' + this.moment.daysInMonth(), 'YYYY-MM-DD').format('x');

      if (thisFrom <= to && from <= thisTo) {
        this.fetchData();
      }
    },

    showPrompt: function (event) {
      const elem = event.currentTarget || event.target;

      this.currentElem = elem;
      this.$refs.prompt.open(elem.dataset, elem.getBoundingClientRect());
    },

    scrollLeft: function (event) {
      const elem = document.getElementById('calendar-data');

      elem.scrollLeft -= 1050;
    },

    scrollRight: function (event) {
      const elem = document.getElementById('calendar-data');

      elem.scrollLeft += 1050;
    },

    updated: function (newValue) {
      this.currentElem.innerText = this.currentElem.dataset.value = newValue;
    },

    addMonth: function (event) {
      this.date = this.moment.add(1, 'months').format('YYYY-MM');
    },

    addYear: function (event) {
      this.date = this.moment.add(1, 'years').format('YYYY-MM');
    },

    subMonth: function (event) {
      this.date = this.moment.subtract(1, 'months').format('YYYY-MM');
    },

    subYear: function (event) {
      this.date = this.moment.subtract(1, 'years').format('YYYY-MM');
    },
  },
});

const App = new Vue({
  el: '#app',

  delimiters: ['[[', ']]'],

  data: {
    fromDate: null,
    toDate: null,
    bulkPrice: null,
    bulkAvailability: null,
    roomType: 'double',
    roomTypes: {
      single: {label: 'Single Room', inventory: 5},
      double: {label: 'Double Room', inventory: 5}
    },
    filterDays: [],
    refineDays: [
      {value: '0,1,2,3,4,5,6', label: 'All days'},
      {value: '1', label: 'Mondays'},
      {value: '4', label: 'Thursdays'},
      {value: '0', label: 'Sundays'},
      {value: '1,2,3,4,5', label: 'All Weekdays'},
      {value: '2', label: 'Tuesdays'},
      {value: '5', label: 'Fridays'},
      {value: '6,0', label: 'All weekends'},
      {value: '3', label: 'Wednesdays'},
      {value: '6', label: 'Saturdays'},
    ],
    storage: new Store('hotel-calendar-'),
    saved: false,
    errors: null,
  },

  methods: {
    clear: function () {
      this.roomType = 'double';
      this.filterDays = [];
      this.fromDate = this.toDate = this.bulkPrice = this.bulkAvailability = null;
    },

    cancel: function () {
      this.clear.call(this);
    },

    validate: function () {
      let errors = [];
      if (isNaN(+this.bulkPrice) || +this.bulkPrice < 1) {
        errors.push('Price must be amount greater than 0');
      }

      const max = this.roomTypes[this.roomType].inventory;
      const num = +this.bulkAvailability;
      if (isNaN(num) || num < 1 || num > max) {
        errors.push('Availability must be a number between 1 and ' + max);
      }

      if (errors.length) {
        this.errors = errors.join('. ');

        return false;
      }

      return true;
    },

    bulkUpdate: function (event) {
      event.preventDefault();

      if (!this.validate.call(this)) {
        return;
      }

      this.errors = null;

      const data = {
        room_type: this.roomType,
        effective_date_from: this.fromDate,
        effective_date_to: this.toDate,
        availability: +this.bulkAvailability,
        price: +this.bulkPrice,
        filter_days: this.filterDays.join(',')
      };

      const self = this;
      Http.call({
        url: BULK_UPDATE_URL, method: 'post', body: JSON.stringify(data)
      }, function () {
        self.updated();
      }, function (error) {
        self.errors = String(error);
      });
    },

    updated: function () {
     this.saved = true;

     const self = this;
     setTimeout(function () { self.saved = false; }, 2500);

     // Reload the calendar if the bulk date ranges overlap!
     this.$refs.calendar.fetchIfOverlap(
       moment(this.fromDate, 'YYYY-MM-DD').format('x'),
       moment(this.toDate, 'YYYY-MM-DD').format('x')
     );

     this.clear();
   }
  }
});
